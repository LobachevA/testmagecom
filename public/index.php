<?php
ini_set('display_errors', 1);
session_start();
require_once 'application/Routes.php';
require_once 'application/DB.php';
require_once 'application/Cart.php';
$DB = DB::getDB();
Route::start();
