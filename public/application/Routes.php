<?php
class Route
{
    static function start()
    {
        $controller_name = 'Main';
        $action_name = 'index';
        $routes = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
// получаем имя контроллера
        if (!empty($routes[0])) {
            $controller_name = ucfirst(strtolower($routes[0]));
            if(!empty($routes[1])){
                if(preg_match('/^\d+$/', $routes[1])){

                    if($controller_name!="Detail"){
                        $action_name = 'unknown';
                    }
                    if($controller_name=="Cart"){
                        $action_name = 'add';
                    }
                } else {if($routes[1]=="clear"){$action_name = 'clear';}else{$action_name = 'unknown';}

                }
            } else { if($controller_name=="Detail"){
                        $action_name = 'unknown';
                    }
            }
        }

// добавляем префиксы
        $controller_name .= 'Controller';
        $action_name .= 'Action';
// подцепляем файл с классом контроллера
        $controller_file =$controller_name.'.php';
        $controller_path = "application/controllers/" . $controller_file;

        if (file_exists($controller_path)) {
            include $controller_path;
        } else {
            Route::ErrorPage404();
        }

// создаем контроллер
        $controller = new $controller_name;
        $action = $action_name;
        if (method_exists($controller, $action)) {
            if($controller_name!="DetailController"){
                if($controller_name!="CartController"){
                    $controller->$action();
                }else{
                    if($action!="indexAction"){
                        $controller->$action($routes[1]);
                    }else{
                    $controller->$action();
                }
                }
            }
            else{
                $controller->$action($routes[1]);
            }

        } else {
            Route::ErrorPage404();
        }

    }

    function ErrorPage404()
    {

        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404.html');
    }
}
