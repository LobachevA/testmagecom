<?php
class Product
{

    public static function getProductList($id)
    {

        $db = DB::getDB();
        $result = $db->querySelect('SELECT * FROM products WHERE status=1 AND category_id='.$id);
        return $result;
    }
    public static function getDetail($id)
    {

        $db = DB::getDB();
        $result = $db->querySelect('SELECT * FROM products WHERE id='.$id);
        return $result;
    }
    public static function getGroup($idsArray)
    {

        $db = DB::getDB();
        $products = array();

        $idsString = implode(',', $idsArray);
        $sql = "SELECT * FROM products WHERE status='1' AND id IN (".$idsString.");";
        $products = $db->querySelect($sql);


        return $products;
    }
}
