<?php include $_SERVER['DOCUMENT_ROOT'].'/' . "application/views/layouts/header.php"; ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-products-->
                        <?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/<?=$categoryItem['name']?>"><?=$categoryItem['name']?></a></h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div><!--/category-products-->


                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Item</h2>
                    <div class="col-xs-6 col-sm-3">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="/<?=$product[0]['image']?>" alt=""/>
                                    <h2><?=$product[0]['name']?></h2>
                                    <p><?=$product[0]['description']?></p>
                                    <a href="/cart/<?=$product[0]['id']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>
<?php include $_SERVER['DOCUMENT_ROOT'].'/' . "application/views/layouts/footer.php"; ?>
