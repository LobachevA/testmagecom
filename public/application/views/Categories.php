<?php include 'layouts/header.php'; ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-products-->
                        <?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/<?=$categoryItem['name']?>"><?=$categoryItem['name']?></a></h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div><!--/category-products-->


                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Items</h2>
                    <?php foreach ($products as $productsItem): ?>
                    <div class="col-xs-6 col-sm-3">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                   <a href="/detail/<?=$productsItem['id']?>"><img src="<?=$productsItem['image']?>" alt=""/></a>
                                    <h2><?=$productsItem['price']?> грн</h2>
                                    <p><?=$productsItem['name']?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>
<?php include 'layouts/footer.php'; ?>