<?php include $_SERVER['DOCUMENT_ROOT'].'/' . "application/views/layouts/header.php"; ?>
    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-9 padding-right">
                    <div class="features_items">
                        <h2 class="title text-center">Корзина</h2>
                        <a href="/cart/clear" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Очистить</a>
                        <?php if ($productsInCart): ?>
                            <p>Вы выбрали такие товары:</p>
                            <table class="table-bordered table-striped table">
                                <tr>
                                    <th>Код товара</th>
                                    <th>Название</th>
                                    <th>Стомость, грн</th>
                                    <th>Количество, шт</th>
                                </tr>
                                <?php foreach ($products as $product): ?>
                                    <tr>
                                        <td><?=$product['id'];?></td>
                                        <td>
                                            <a href="/detail/<?=$product['id'];?>">
                                                <?=$product['name'];?>
                                            </a>
                                        </td>
                                        <td><?=$product['price'];?></td>
                                        <td><?=$productsInCart[$product['id']];?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td colspan="3">Общая стоимость:</td>
                                    <td><?=$totalPrice;?></td>
                                </tr>

                            </table>
                        <?php else: ?>
                            <p>Корзина пуста</p>
                        <?php endif; ?>

                    </div>
                    <form action="" method="post">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <input type="submit" name="submit" class="btn btn-default" value="Оформить" />
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include $_SERVER['DOCUMENT_ROOT'].'/' . "application/views/layouts/footer.php"; ?>