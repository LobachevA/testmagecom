<?php include 'layouts/header.php'; ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-products-->
                        <?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/<?=$categoryItem['name']?>"><?=$categoryItem['name']?></a></h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div><!--/category-products-->


                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items">
                        <h2 class="title text-center">Welcome to our shop</h2>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'layouts/footer.php'; ?>