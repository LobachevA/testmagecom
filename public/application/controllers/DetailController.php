<?php
include_once 'application/models/Category.php';
include_once 'application/models/Product.php';
class DetailController {
    public function indexAction($id){
        $categories = array();
        $categories = Category::getCategoriesList();
        $product = array();
        $product = Product::getDetail($id);
        require_once('application/views/Detail.php');
    }
}