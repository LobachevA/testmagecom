<?php
include_once 'application/models/Product.php';
include_once 'application/Cart.php';
class CartController
{

    public function addAction($id)
    {
        // Добавляем товар в корзину
        Cart::addProduct($id);

        // Возвращаем пользователя на страницу
        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: $referrer");
    }

    public function clearAction()
    {
        // Удалить товар из корзины
        Cart::clear();
        // Возвращаем пользователя на страницу
        header("Location: /");
    }

    public function indexAction()
    {

        $productsInCart = false;

        // Получим данные из корзины
        $productsInCart = Cart::getProducts();

        if ($productsInCart) {
            // Получаем полную информацию о товарах для списка
            $productsIds = array_keys($productsInCart);
            $products = Product::getGroup($productsIds);

            // Получаем общую стоимость товаров
            $totalPrice = Cart::getTotalPrice($products);
        }
        if (isset($_POST['submit']) and isset($_POST['email'])) {
            // Форма отправлена? - Да
            // Считываем данные формы
            $DB = DB::getDB();
            $userEmail = $_POST['email'];
            $result = $DB::add($userEmail, $productsInCart);
            if ($result) {
                    Cart::clear();
                header("Location: /");
                }
        }


        require_once('application/views/FullCart.php');

    }


}