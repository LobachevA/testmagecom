<?php
include_once 'application/models/Category.php';
include_once 'application/models/Product.php';
class FridgeController {
    public function indexAction(){
        $categories = array();
        $categories = Category::getCategoriesList();
        $products = array();
        $products = Product::getProductList(2);
        require_once('application/views/Categories.php');
    }
}