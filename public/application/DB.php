<?php

class DB {
    private static $db = null;

    public static function getDB() {
        if (self::$db == null) self::$db = new DB();
        return self::$db;
    }

    private function __construct(){
        $this->db = new PDO('mysql:host=localhost;dbname=testmagecom','root','alexlobo');

    }

    public function querySelect($query){
        $dbRes = $this->db->query($query);

        $dbRes->setFetchMode(PDO::FETCH_ASSOC);

        $arItems = [];
        while ($row = $dbRes->fetch()){
            $arItems[] = $row;
        }
        return $arItems;
    }

    public function add($useremail, $products){
        $dbb=new PDO('mysql:host=localhost;dbname=testmagecom','root','alexlobo');
        $products = json_encode($products);

        $sql = 'INSERT INTO orders (useremail, products) VALUES (:useremail, :products)';

        $result = $dbb->prepare($sql);
        $result->bindParam(':useremail', $useremail, PDO::PARAM_STR);
        $result->bindParam(':products', $products, PDO::PARAM_STR);
        return $result->execute();

    }

    public function disconnect(){
        $this->db = null;
    }

    public function __destruct(){
        $this->disconnect();
    }
}